﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessGoBoxes
{
    class Data_Handler
    {

        public static List<gb_boxtypes> GetBoxTypes()
        {
            SPAPPS_Entity db = new SPAPPS_Entity();

            return db
                .gb_boxtypes
                .ToList();
        }

        public static void AddBoxType(string description)
        {
            SPAPPS_Entity db = new SPAPPS_Entity();
            gb_boxtypes new_type = new gb_boxtypes();
            new_type.Description = description;
            db.gb_boxtypes.Add(new_type);
            db.SaveChanges();
    
        }

        public static void RemoveBoxType(string description)
        {
            SPAPPS_Entity db = new SPAPPS_Entity();
            gb_boxtypes remove_type = db.
                gb_boxtypes
                .Where(
                        m => m.Description.Equals(description)
                        )
                        .SingleOrDefault();

            db.gb_boxtypes.Remove(remove_type);
            db.SaveChanges();

        }
    }
}
