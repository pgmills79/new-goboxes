﻿namespace ProcessGoBoxes
{
    partial class frmGoBoxes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cboBoxTypes = new System.Windows.Forms.ComboBox();
            this.cms_BoxTypeControl = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addGoBoxTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeGoBoxTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblGoBoxType = new System.Windows.Forms.Label();
            this.cms_BoxTypeControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboBoxTypes
            // 
            this.cboBoxTypes.ContextMenuStrip = this.cms_BoxTypeControl;
            this.cboBoxTypes.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBoxTypes.FormattingEnabled = true;
            this.cboBoxTypes.Location = new System.Drawing.Point(12, 40);
            this.cboBoxTypes.Name = "cboBoxTypes";
            this.cboBoxTypes.Size = new System.Drawing.Size(194, 24);
            this.cboBoxTypes.TabIndex = 1;
            this.cboBoxTypes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cboBoxTypes_MouseDown);
            // 
            // cms_BoxTypeControl
            // 
            this.cms_BoxTypeControl.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addGoBoxTypeToolStripMenuItem,
            this.removeGoBoxTypeToolStripMenuItem});
            this.cms_BoxTypeControl.Name = "cms_BoxTypeControl";
            this.cms_BoxTypeControl.Size = new System.Drawing.Size(186, 48);
            // 
            // addGoBoxTypeToolStripMenuItem
            // 
            this.addGoBoxTypeToolStripMenuItem.Name = "addGoBoxTypeToolStripMenuItem";
            this.addGoBoxTypeToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.addGoBoxTypeToolStripMenuItem.Text = "&Add Go Box Type";
            this.addGoBoxTypeToolStripMenuItem.Click += new System.EventHandler(this.addGoBoxTypeToolStripMenuItem_Click);
            // 
            // removeGoBoxTypeToolStripMenuItem
            // 
            this.removeGoBoxTypeToolStripMenuItem.Name = "removeGoBoxTypeToolStripMenuItem";
            this.removeGoBoxTypeToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.removeGoBoxTypeToolStripMenuItem.Text = "&Remove Go Box Type";
            this.removeGoBoxTypeToolStripMenuItem.Click += new System.EventHandler(this.removeGoBoxTypeToolStripMenuItem_Click);
            // 
            // lblGoBoxType
            // 
            this.lblGoBoxType.AutoSize = true;
            this.lblGoBoxType.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoBoxType.Location = new System.Drawing.Point(13, 21);
            this.lblGoBoxType.Name = "lblGoBoxType";
            this.lblGoBoxType.Size = new System.Drawing.Size(95, 14);
            this.lblGoBoxType.TabIndex = 5;
            this.lblGoBoxType.Text = "Go Box Type:";
            // 
            // frmGoBoxes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 285);
            this.Controls.Add(this.lblGoBoxType);
            this.Controls.Add(this.cboBoxTypes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmGoBoxes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Process Go Boxes";
            this.Load += new System.EventHandler(this.frmGoBoxes_Load);
            this.cms_BoxTypeControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboBoxTypes;
        private System.Windows.Forms.ContextMenuStrip cms_BoxTypeControl;
        private System.Windows.Forms.ToolStripMenuItem addGoBoxTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeGoBoxTypeToolStripMenuItem;
        private System.Windows.Forms.Label lblGoBoxType;
    }
}

