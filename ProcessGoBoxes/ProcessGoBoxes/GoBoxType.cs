﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcessGoBoxes
{
    public partial class GoBoxType : Form
    {
        public GoBoxType()
        {
            InitializeComponent();
        }

        private string BoxTypeAction { get; set; }
        private string BoxTypeDescription { get; set; }

        private const string AddGoBoxType = "Add Go Box Type";

        private const string RemoveGoBoxType = "Remove Go Box Type";

        public GoBoxType(string action, string boxtype = "")
        {
            InitializeComponent();
            BoxTypeAction = action;
            BoxTypeDescription = boxtype;

            if (BoxTypeAction.Equals("remove"))
            {
                btnUpdateGoBoxTypes.Text = RemoveGoBoxType;
                txtGoBoxType.Text = BoxTypeDescription;
                txtGoBoxType.ReadOnly = true;
            }

            if (BoxTypeAction.Equals("add"))
            {
                btnUpdateGoBoxTypes.Text = AddGoBoxType;
                txtGoBoxType.Text = BoxTypeDescription;
                txtGoBoxType.ReadOnly = false;
            }

        }

        private void txtGoBoxType_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar)
                && !char.IsControl(e.KeyChar)
                && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;          
                
        }

        private void txtGoBoxType_TextChanged(object sender, EventArgs e)
        {
            if (txtGoBoxType.TextLength.Equals(0))
                btnUpdateGoBoxTypes.Visible = false;
            else
                btnUpdateGoBoxTypes.Visible = true;
        }     

        

        private void GoBoxType_Load(object sender, EventArgs e)
        {
           
        }

        private void btnUpdateGoBoxTypes_Click(object sender, EventArgs e)
        {

            if (btnUpdateGoBoxTypes.Text.Equals(AddGoBoxType))
            {
                Data_Handler.AddBoxType(txtGoBoxType.Text);
            }

            if (btnUpdateGoBoxTypes.Text.Equals(RemoveGoBoxType))
            {
                Data_Handler.RemoveBoxType(txtGoBoxType.Text);
            }

            this.Close();
        }

        private void btnCancelAddBoxType_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
