﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcessGoBoxes
{
    public partial class frmGoBoxes : Form
    {

        private int NumberOfBoxTypes { get; set; }
        public frmGoBoxes()
        {
            InitializeComponent();
        }

        private void frmGoBoxes_Load(object sender, EventArgs e)
        {

            foreach (gb_boxtypes item in Data_Handler.GetBoxTypes())
            {
                cboBoxTypes.Items.Add(item.Description);
            }

            NumberOfBoxTypes = cboBoxTypes.Items.Count;

            if (NumberOfBoxTypes > 0) { cboBoxTypes.SelectedIndex = 0; }
        }

       

        private void addGoBoxTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GoBoxType add_boxtype = new GoBoxType("add");
            //show the Box Type Dialog box
            add_boxtype.ShowDialog();

            //after done with dialog box clear items and re-load them if the count of
            //items is different (we don't want to refresh item list if no changes 
            //were made
            List<gb_boxtypes> boxtype_items = Data_Handler.GetBoxTypes();

            if (boxtype_items.Count > NumberOfBoxTypes)
            {
                cboBoxTypes.Items.Clear();
                foreach (gb_boxtypes item in boxtype_items)
                {
                    cboBoxTypes.Items.Add(item.Description);
                }

                cboBoxTypes.SelectedIndex = (cboBoxTypes.Items.Count - 1);
                NumberOfBoxTypes = cboBoxTypes.Items.Count;
            }
        }

        private void removeGoBoxTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {

            GoBoxType remove_boxtype = new GoBoxType("remove", 
                                        cboBoxTypes.SelectedText);

            //show the Box Type Dialog box
            remove_boxtype.ShowDialog();

            //after done with dialog box clear items and re-load them if the count of
            //items is different (we don't want to refresh item list if no changes 
            //were made
            List<gb_boxtypes> boxtype_items = Data_Handler.GetBoxTypes();

            if (boxtype_items.Count < NumberOfBoxTypes && boxtype_items.Count > 0)
            {
                cboBoxTypes.Items.Clear();
                foreach (gb_boxtypes item in boxtype_items)
                {
                    cboBoxTypes.Items.Add(item.Description);
                }

                cboBoxTypes.SelectedIndex = (cboBoxTypes.Items.Count - 1);
                NumberOfBoxTypes = cboBoxTypes.Items.Count;
            }

        }

        private void cboBoxTypes_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Right))
            {
                //we don't want to allow anymore removal of items if 
                //there is only one box type in the system
                if (NumberOfBoxTypes < 2)
                { removeGoBoxTypeToolStripMenuItem.Visible = false; }
             
            }
        }
    }
}
