﻿namespace ProcessGoBoxes
{
    partial class GoBoxType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtGoBoxType = new System.Windows.Forms.TextBox();
            this.btnUpdateGoBoxTypes = new System.Windows.Forms.Button();
            this.btnCancelAddBoxType = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtGoBoxType
            // 
            this.txtGoBoxType.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoBoxType.Location = new System.Drawing.Point(186, 25);
            this.txtGoBoxType.Name = "txtGoBoxType";
            this.txtGoBoxType.Size = new System.Drawing.Size(142, 24);
            this.txtGoBoxType.TabIndex = 0;
            this.txtGoBoxType.TextChanged += new System.EventHandler(this.txtGoBoxType_TextChanged);
            this.txtGoBoxType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGoBoxType_KeyPress);
            // 
            // btnUpdateGoBoxTypes
            // 
            this.btnUpdateGoBoxTypes.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateGoBoxTypes.Location = new System.Drawing.Point(27, 23);
            this.btnUpdateGoBoxTypes.Name = "btnUpdateGoBoxTypes";
            this.btnUpdateGoBoxTypes.Size = new System.Drawing.Size(142, 27);
            this.btnUpdateGoBoxTypes.TabIndex = 1;
            this.btnUpdateGoBoxTypes.Text = "Update Go Box Types";
            this.btnUpdateGoBoxTypes.UseVisualStyleBackColor = true;
            this.btnUpdateGoBoxTypes.Visible = false;
            this.btnUpdateGoBoxTypes.Click += new System.EventHandler(this.btnUpdateGoBoxTypes_Click);
            // 
            // btnCancelAddBoxType
            // 
            this.btnCancelAddBoxType.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelAddBoxType.Location = new System.Drawing.Point(106, 82);
            this.btnCancelAddBoxType.Name = "btnCancelAddBoxType";
            this.btnCancelAddBoxType.Size = new System.Drawing.Size(142, 27);
            this.btnCancelAddBoxType.TabIndex = 2;
            this.btnCancelAddBoxType.Text = "&Cancel";
            this.btnCancelAddBoxType.UseVisualStyleBackColor = true;
            this.btnCancelAddBoxType.Click += new System.EventHandler(this.btnCancelAddBoxType_Click);
            // 
            // GoBoxType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 130);
            this.Controls.Add(this.btnCancelAddBoxType);
            this.Controls.Add(this.btnUpdateGoBoxTypes);
            this.Controls.Add(this.txtGoBoxType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "GoBoxType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GoBoxType";
            this.Load += new System.EventHandler(this.GoBoxType_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtGoBoxType;
        private System.Windows.Forms.Button btnUpdateGoBoxTypes;
        private System.Windows.Forms.Button btnCancelAddBoxType;
    }
}